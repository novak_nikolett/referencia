//VÉRADÁS NYILVÁNTARTÓ PROGRAM _ PROGMÓD BEADANDÓ - NOVÁK NIKOLETT
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "PROGMOD_NN.h"

using namespace std;


int main(/*int argc, char **argv */) {
/*    if(argc == 2){
		my_strcpy(filename,argv[1]);
		return 0;
	}*/
    open();
    menu();

return 0;
}
//-----------------------
void open(){

    f=fopen("adat.bin", "r+b"); //megnyitja vagy létrehozza az adatokat tároló file-t
        if(f==NULL){
            f=fopen("adat.bin", "w+b");
            if(f==NULL){              //ellenõrzi
                printf("Hiba lepett fel a file megnyitasa soran!\n");
                system("pause");
            }
        }
        else{
            fseek(f, 0, SEEK_END);
        }
        fseek(f, 0, SEEK_SET);
        fread(&data, sizeof(database), 1, f);
    //  fscanf(f, "%d %s %s %s %d %s", &data.id, data.name, data.blood_type, data.mail, &data.times, data.last_date); //betölti a dokumentumban lévõ adatokat
        while(!feof(f)){    //betölti a dokumentumban lévõ adatokat
            New=(struct database*)malloc(sizeof(database));
            if(New==NULL){
                printf("Memoria hiba!\n");
                system("pause");
            }
            New->id=data.id;
            my_strcpy(New->name,data.name);
            my_strcpy(New->blood_type,data.blood_type);
            my_strcpy(New->mail,data.mail);
            New->times=data.times;
            my_strcpy(New->last_date,data.last_date);

            New->next=NULL;
            if(first==NULL){
                first=New;
            }
            else{
                actual->next=New;
            }
            actual=New;
    //      fscanf(f, "%d %s %s %s %d %s", &data.id, data.name, data.blood_type, data.mail, &data.times, data.last_date);
            fread(&data, sizeof(database), 1, f);
        }
        printf("Az adatbazis betoltve a megfelelo file-bol!\n");
        printf("Kerem, adja meg a mai datumot! (0000.00.00.)\t"); //mai dátumot bekéri és feltördeli
        fflush(stdin);
        scanf("%s", date);
        fflush(stdin);
        int w=count_of_days(date);
        while(w<0){
            if(w==-1){
                printf("Rossz datumformatumot adott meg vagy false datumot! (A helyes formatum: 0000.00.00.)\t");
            }
            else if(w==-2){
                printf("Ez a honap max 31 napos!Javitsa!\t");
            }
            else if(w==-3){
                printf("Ez a honap max 30 napos! Javitsa!\t");
            }
            else if(w==-4){
                printf("Ez a honap max 28 napos! Javitsa!\t");
            }
            fflush(stdin);
            scanf("%s", date);
            w=count_of_days(date);
        }
        if(w>=0){
            printf("A mai datum konvertalva!\n");
            a=count_of_days(date);
        }

        printf("A program keszen all a hasznalatra!\n");
        printf("Nyomjon ENTER-t a kezdeshez!\n");
        fflush(stdin);
        getchar();
}
int menu(){
char cmd='0';
	do{
		fflush(stdin);
		system("cls");
		printf("=====================================================================================\n");
		printf("|VALASSZON AZ ALABBI MENUPONTOKBOL!                                                 |\n");
		printf("=====================================================================================\n");
		printf("|1. Uj verado felvetele                                                             |\n");
		printf("|2. Vertipus keresese + verado automatikus felkerese veradasra                      |\n");
		printf("|3. Listazas                                                                        |\n");
		printf("|4. Kilepes                                                                         |\n");
		printf("=====================================================================================\n");

		cmd = getchar();
		switch(cmd){
			case '1': { new_data(); }
				break;
			case '2': { search_data(); }
				break;
			case '3': { list_database(); }
				break;
            case '4': {
				return close(); }
				break;
			default: {
				if(cmd != '0'){
                    system("cls");
                    printf("|=====================================================================================\n");
                    printf("|ERVENYTELEN UTASITAS! NYOMJON ENTER-T A FOLYTATASHOZ!                               |\n");
                    printf("|=====================================================================================\n");
                    fflush(stdin);
                    getchar();
				}
				break;
			}
		}
		fflush(stdin);
	}while(cmd != '4');
}
void new_data(){
system("cls");
	printf("=====================================================================================\n");
	printf("|Az On altal valasztott opcio: Uj verado felvetele                                  |\n");
	printf("=====================================================================================\n");
	printf("Adja meg az azonositot!(Csak egesz szam lehet!)\n");
	fflush(stdin);
	scanf("%d", &new_id);

	actual=first;
	while(actual){
		if(new_id==actual->id){	//ellenőrzés az azonosítóra, létezik-e már
			printf("Ilyen azonosito mar szerepel az adatbazisban!\n");
			break;
		}
        actual=actual->next;
	}
    if(actual==NULL){
        printf("Adja meg a verado nevet!\n");
        fflush(stdin);
        scanf("%[^\n]", new_name);
        printf("Adja meg a verado vertipusat!\n");
        fflush(stdin);
        scanf("%[^\n]", new_blood_type);
        printf("Adja meg a verado e-mail cimet!\n");
        do{
            fflush(stdin);
            scanf("%[^\n]", new_mail);
        }
        while( !(check_1(new_mail)==1) || !(check_2(new_mail)==2) || !(check_3(new_mail)==3) || !(check_4(new_mail)==4) );

        printf("Adja meg a verado veradasainak szamat! \n");
        fflush(stdin);
        scanf("%d", &new_times);
        printf("Adja meg a verado utolso veradasi idopontjat! (0000.00.00.)\n");
        fflush(stdin);
        scanf("%[^\n]", new_last_date);
            int w=count_of_days(new_last_date);
                while(w<0){
                    if(w==-1){
                        printf("Rossz datumformatumot adott meg vagy false datumot! (A helyes formatum: 0000.00.00.)\t");
                    }
                    else if(w==-2){
                        printf("Ez a honap max 31 napos!Javitsa!\t");
                    }
                    else if(w==-3){
                        printf("Ez a honap max 30 napos! Javitsa!\t");
                    }
                    else if(w==-4){
                        printf("Ez a honap max 28 napos! Javitsa!\t");
                    }
                fflush(stdin);
                scanf("%s", new_last_date);
                w=count_of_days(new_last_date);
            }
            if(w>=0){

                New=(struct database*)malloc(sizeof(database)); //helyfoglalás:
                if(!New){   //ellenorzés:
                    printf("Memoria hiba!\n");
                    system("pause");
                }
                New->id=new_id;
                my_strcpy(New->name,new_name);
                my_strcpy(New->blood_type,new_blood_type);
                my_strcpy(New->mail,new_mail);
                New->times=new_times;
                my_strcpy(New->last_date,new_last_date);

                actual=first;
                last=NULL;
                while(actual&&new_id>actual->id){  //rendezés azonosítóra
                    last=actual;
                    actual=actual->next;
                }
                if(!last){
                first=New;
                }
                else{
                    last->next=New;
                }
                New->next=actual;

                fflush(stdin);
                printf("Sikeres adatfelvitel!\n");
            }
    }
    printf("Nyomjon ENTER-t a folytatashoz!");
    fflush(stdin);
    getchar();
}
void search_data(){
    system("cls");
	printf("=====================================================================================\n");
	printf("|Az On altal valasztott opcio: Kereses vertipusnak megfeleloen                      |\n");
	printf("=====================================================================================\n");
	printf("Adja meg milyen vertipusnak megfeleloen keressen az adatbazis!  ");
	fflush(stdin);
    scanf("%[^\n]", new_blood_type);

	actual=first;
	while(actual){
        if(actual==NULL){
            printf("A keresett vercsoporthoz nem talalhato megfelelo verado!\n");
        }
        if(my_search(new_blood_type,actual->blood_type) == 0){
            b=count_of_days(actual->last_date);
            if((c=a-b)>90){ //hiába egyezik a vércsoport, 90 napon belül tilos ismét vért adni!
                printf("Verado neve: %s\n", actual->name);
                printf("Vertipusa %s\t", actual->blood_type);
                printf("A verado elerhetosege: %s\n\n", actual->mail);

                actual->times++;    //megelőlegezvén a bizalmat
                my_strcpy(actual->last_date,date);
                printf("A veradot ertesitette a rendszer, \"elment vert adni\"...\n");
            }
        }
        actual=actual->next;
	}
	printf("Nyomjon ENTER-t a folytatashoz!");
    fflush(stdin);
	getchar();
}
void list_database(){
    system("cls");
	printf("=====================================================================================\n");
	printf("|Adatbazis listazasa...                                                             |\n");
	printf("=====================================================================================\n\n");
	printf("| Azonosito |      Verado neve      | Vertipus | Verado e-mail cime | Veradasok szama | Utoso veradas datuma |\n");
	printf("______________________________________________________________________________________________________________\n\n");

	actual=first;
    while(actual){
        printf("%10d\t %12s\t %12s\t %5s\t %15d\t %10s\n",actual->id, actual->name, actual->blood_type, actual->mail, actual->times, actual->last_date);
            if(count_of_days(actual->last_date)==-1){
                printf("A datumformatum hibas!!!\n");
            }
        actual=actual->next;

	}
	printf("Nyomjon ENTER-t a folytatashoz!");
    fflush(stdin);
	getchar();
}
int close(){
    fseek(f, 0, SEEK_SET);
    actual=first;
    while(actual){
    // fprintf(f, "%d %s %s %s %d %s\n", data.id, data.name, data.blood_type, data.mail, data.times, data.last_date);
        fwrite(actual, sizeof(data), 1, f);
		actual=actual->next;
    }
    fclose(f);
    printf("Az adatok mentve az eredeti file-ba! Sikeres kilepes! Viszlat!\n");
return 0;
}
int count_of_days(char *date){
int date_now;
    if((date[4]!='.') || (date[7]!='.') || (date[10]!='.') || ((date[5]-'0')>1) || ((date[0]-'0')==0 && (date[1]-'0')==0 && (date[2]-'0')==0 && (date[3]-'0')==0)|| ((date[5]-'0')==0 && (date[6]-'0')==0) ||((date[5]-'0')==1 && (date[6]-'0')>2) || ((date[8]-'0')==0 && (date[9]-'0')==0)){ // kizárja a false hónapot is!
        return -1;
    }
    //szűrés 31-napos hónapokra:
    else if(((date[5]-'0')==0 && (date[6]-'0')==1 && (date[8]-'0')==3 && (date[9]-'0')>1) || ((date[5]-'0')==0 && (date[6]-'0')==3 && (date[8]-'0')==3 && (date[9]-'0')>1) || ((date[5]-'0')==0 && (date[6]-'0')==5 && (date[8]-'0')==3 && (date[9]-'0')>1) || ((date[5]-'0')==0 && (date[6]-'0')==7 && (date[8]-'0')==3 && (date[9]-'0')>1) || ((date[5]-'0')==0 && (date[6]-'0')==8 && (date[8]-'0')==3 && (date[9]-'0')>1) || ((date[5]-'0')==1 && (date[6]-'0')==0 && (date[8]-'0')==3 && (date[9]-'0')>1) || ((date[5]-'0')==1 && (date[6]-'0')==2 && (date[8]-'0')==3 && (date[9]-'0')>1)){
        return -2;
    }
    //szűrés 30-napos hónapra:
    else if(((date[5]-'0')==0 && (date[6]-'0')==4 && (date[8]-'0')==3 && (date[9]-'0')>0) || ((date[5]-'0')==0 && (date[6]-'0')==6 && (date[8]-'0')==3 && (date[9]-'0')>0) || ((date[5]-'0')==0 && (date[6]-'0')==9 && (date[8]-'0')==3 && (date[9]-'0')>0) || ((date[5]-'0')==1 && (date[6]-'0')==1 && (date[8]-'0')==3 && (date[9]-'0')>0)){
        return -3;
    }
    //magányos február:
    else if((date[5]-'0')==0 && (date[6]-'0')==2 && (date[8]-'0')==2 && (date[9]-'0')>8){
        return -4;
    }
    // a jó megoldás:
    else{
        int year=(date[3]-'0')+10*(date[2]-'0')+100*(date[1]-'0')+1000*(date[0]-'0');
        int month=(date[6]-'0')+10*(date[5]-'0');
        int day=(date[9]-'0')+10*(date[8]-'0');
        return date_now=day+30*month+365*year;
    }
}
char my_strcpy(char *new_s, char *original_s){
    int i=0;
    while(original_s[i]!='\0'){
        new_s[i]=original_s[i];
        i++;
    }
    new_s[i]='\0';
   // return new_s;
}
int my_search(const char *s1, const char *s2){
    while(*s1 && (*s1 == *s2)){
        s1++;
        s2++;
    }
    return *(const unsigned char *)s1 - *(const unsigned char *)s2;
}
int check_1(char *new_mail){
    int j=0;
    while(new_mail[j]!='\0'){
        if(new_mail[j]!='.') {
            j++;
        }
        else return 1;
    }
        if(new_mail[j]=='\0'){
            printf("Az e-mail cimnek tartalmaznia kell pontot! Adjon meg helyes cimet!\n");
            return 3;
        }
}
int check_2(char *new_mail){
    int i=0;
        while(new_mail[i]!='\0'){
            if(new_mail[i]==' '){
                printf("Az e-mail cim nem tartalmazhat szokozt! Adjon meg helyes e-mailt!\n");
                return 4;
            }
            else if((new_mail[i]=='.' && new_mail[i-1]=='@') || (new_mail[i]=='@' && new_mail[i-1]=='.')){
                printf("Az e-mailben nem allhat egymas mellett a pont es a @! Probalja ujra!\n");
                return 5;
            }
            else i++;

        }
        if(new_mail[i]=='\0') return 2;
}
int check_3(char *new_mail){
    int j=0;
    while(new_mail[j]!='\0'){
        if(new_mail[j]!='@'){
            j++;
        }
        else if (new_mail[j]=='@') return 3;
    }
        if(new_mail[j]=='\0'){
            printf("Az e-mail cimnek tartalmaznia kell kukacot! Adjon meg helyes cimet!\n");
            return 6;
        }
}
int check_4(char *new_mail){
    int i=0;
    while(new_mail[i]!='\0'){
            i++;
    }
    if(new_mail[i-1]=='@' || new_mail[i-1]=='.'){
        printf("Nem allhat az e-mail cim vegen a pont vagy a @ karakter! Irjon be helyes mail cimet!\n");
        return 8;
    }
    else return 4;
}
