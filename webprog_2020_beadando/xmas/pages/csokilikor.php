<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="UTF-8">
	<title>Finom csokilikőr recept</title>

	<link rel="stylesheet" type="text/css" href="csokilikor_style.css">
	
</head>
	<body>
		<header>
			<h1>Csokilikőr</h1>
		</header>
			<main>		
					<section>
						<h2>Hozzávalók</h2>
						<p>
						<nav>
							<ul>
								<li>80 dkg kristálycukor</li>
								<li>10 dkg cukrozatlan kakaópor</li>
								<li>1 csomag vaníliás cukor</li>
								<li>1 üveg sör</li>
								<li>2 dl tiszta szesz</li>
								<li>2 dl rum</li>
							</ul>
						</nav>
						</p>
					</section>
					<section>
						<h2>Elkészítés</h2>						
							<p>A kristálycukrot a kakaót és a vaníliás cukrot összekeverjük, és a sörrel addíg melegítjük, amíg a cukor fel nem olvad teljesen.</p>
							<p>Ezután az alkoholokat beleöntjük, és levesszük a tűzről.</p>
							<p>Ha kihűlt, üvegbe töltjük, és hűtőben tároljuk.</p>						
					</section>
			</main>	
		<footer>
			<p></p>
		</footer>
	</body>
</html>