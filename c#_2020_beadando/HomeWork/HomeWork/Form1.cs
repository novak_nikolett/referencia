﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeWork
{
    public partial class Form1 : Form // 2. űrlap
    {
        private string name, date, food, color;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void goBtn_Click(object sender, EventArgs e)
        {
            // betáplált adatok mentése
            name = fName.Text;
            date = bornDate.Text;
            food = favFood.Text;
            color = favCol.Text;

            // bekért szültési dátum kezelése
            string[] born = date.Split('.');

            // ha megfelel a kritériumoknak
            if (name.Length >= 3 && food.Length >= 3 && color.Length >= 3 && born[0].Length == 4 && born[1].Length > 0 && born[1].Length <= 2 && born[2].Length > 0 && born[2].Length <= 2)
            {
                int bornYear = Int32.Parse(born[0]);
                int bornMonth = Int32.Parse(born[1]);
                int bornDay = Int32.Parse(born[2]);

                if(bornYear >= 1900 && bornYear <= 2020 && bornMonth >= 1 && bornMonth <= 12 && bornDay >= 1 && bornDay <= 31)
                {
                    this.Hide();
                    Model model = new Model(name, bornYear, bornMonth, bornDay, food, color);
                }
                else MessageBox.Show("Kérlek, helyesen add meg az adatokat!");
            }

            else MessageBox.Show("Kérlek, helyesen add meg az adatokat!");
        }
    }
}
