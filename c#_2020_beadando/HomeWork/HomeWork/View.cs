﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    class View // 5. kinézet felépítése, elemek működtetése
    {
        private Form2 form2;

        private string rizsa;
        public View(string name, string planet1, string planet2, string astral, string randomText, int years, bool bD, string dateNow)
        {
            // szülinapos esetén
            if(bD)  rizsa = "Kedves " + name + "!\n\nBoldog " + years + ". születésnapot!\nA csillagok állásából, és ahogy a hónapban a(z) " + planet1 + "\nközelíti meg a(z) " + planet2 + "-t, a csillagjegyedhez(" + astral + ")\ntartozó e napi horoszkópod a következő:\n\n\n" + randomText;

            else    rizsa = "Kedves " + name + "!\n\n A csillagok állásából, és ahogy a hónapban a(z) " + planet1 + "\nközelíti meg a(z) " + planet2 + "-t, a csillagjegyedhez(" + astral + ")\ntartozó e napi horoszkópod a következő:\n\n\n" + randomText;


            form2 = new Form2(rizsa, dateNow);
            form2.ShowDialog();
        }
    }
}
