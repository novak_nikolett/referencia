<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="UTF-8">
	<title>Christmas is coming</title>

	<link rel="stylesheet" type="text/css" href="src/style.css">	
	
</head>
	<body>
					<ul class="gallery">
						<li>
							<a href="pages/tojaslikor.php"> 
								<img src="res/adventi_koszoru.jpg" alt="Adventi koszorú">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/disz.jpg" alt="Karácsonyfadísz">
							</a>
						</li>
						<li>
							<a href="pages/karacsonyfa_diszites.php">
								<img src="res/diszites.jpg" alt="Karácsonyfa díszítés">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/karacsonyfa.jpg" alt="Feldíszített karácsonyfa">
							</a>
						</li>
						<li>
							<a href="pages/mezeskalacs.php">
								<img src="res/mezeskalacs.jpg" alt="Mézeskalács">
							</a>
						</li>
						<li>
							<a href="pages/csokilikor.php">
								<img src="res/likorok.jpg" alt="Likőr">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/mezeskalacs_3.jpg" alt="Még több mézeskalács">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/tojaslikor.jpg" alt="Tojáslikőr">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/vasar.jpg" alt="Vásár">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/adventi_koszoru_2.jpg" alt="Advent">
							</a>
						</li>
						<li>
							<a href="pages/vendegkonyv.php">
								<img src="res/vasar_3.jpg" alt="Győri vásár">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/vasar_4.jpg" alt="Pécs">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/mezeskalacs_2.jpg" alt="Még több mézeskalács">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/vasar_2.jpg" alt="Győr">
							</a>
						</li>
						<li>
							<a href="pages/countdown.php">
								<img src="res/winter.jpg" alt="Tél">
							</a>
						</li>
						<li>
							<a href="pages/vendegkonyv.php">
								<img src="res/tuzijatek.jpg" alt="Tüzijáték">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/szaloncukor.jpg" alt="Szaloncukor">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/grincs.jpg" alt="Grincsfa">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/gift.jpg" alt="Ajándékok">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/friends.jpg" alt="Barátok">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/forrocsoki_2.jpg" alt="Könyvek">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/forrocsoki.jpg" alt="Forrócsoki">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/book.jpg" alt="Könyv">
							</a>
						</li>
						<li>
							<a href="pages/tojaslikor.php">
								<img src="res/santas.jpg" alt="Mikuláscsokik">
							</a>
						</li>
					</ul>
					

	</body>
</html>