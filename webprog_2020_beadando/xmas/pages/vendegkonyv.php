<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="UTF-8">
	<title>Vendégkönyv</title>

	<link rel="stylesheet" type="text/css" href="vendegkonyv_style.css">

</head>
	<body>
		<header>
			<h1>Vendégkönyv</h1>
		</header>
			<main>
				<section>
					<h2>Írj nekünk!</h2>
						<p>Írd le a kedvenc karácsonyi versedet, sütireceptedet, a legjobb szaloncukor-lelőhelyet, vagy bármit, ami az ünnephez kapcsolódik, és mi betesszük a következő évi naptárunkba!</p>
				</section>

<?php

$dbName = "nino96n";
$dbUser = "nino96n";
$dbPass = "30stm18.";

$dsn = "mysql:host=localhost;dbname=".$dbName.";charset=utf8mb4";

$db = new PDO($dsn, $dbUser, $dbPass);




	if(isset($_POST['sendEmail'])){
		$name  = $_POST['name'];
		$email = $_POST['email'];

		$name  = trim($name);
		$email = trim($email);

		if(strlen($name) > 0 && strlen($email) > 0){
			echo '<p class="success">Kedves '.$name.'! Köszönjük, hogy írtál a vendégkönyvbe!</p>';
			
			$sql = "INSERT INTO customerbook VALUES (NULL, :name,:email,:rating,:message,0);";

			$query = $db->prepare($sql);
			$query->execute([
				'name' => $_POST['name'],
				'email' => $_POST['email'],
				'rating' => $_POST['subject'],
				'message' => $_POST['message'],
			]);
			
		}
		else{
			echo '<p class="error">Hibásan töltötted ki az ürlapot!</p>';
		}
	}		
						
?>
					<section id="block">
						<form method="post" action="">
							<p>
							<label for="inputName">Az Ön neve</label>
							<input type="text" name="name" id="inputName" maxlength="50">
							</p>
							<p>
							<label for="inputEmail">E-mail címe</label>
							<input type="email" name="email" id="inputEmail" maxlength="256">
							</p>
							<p>
							<label for="inputSubject">Üzenet tartalma</label>
							<select name="subject" id="inputSubject">
								<option value="3">Vers/Mese</option>
								<option value="2">Recept</option>
								<option value="1">Beszámoló</option>
								<option value="0">Egyéb</option>
							</select>
							</p>
							<p>
							<label for="inputMessage">Üzenet szövege</label>
							<textarea name="message" id="inputMessage" maxlength="1000"></textarea>
							</p>
							<p>
							<input type="submit" name="sendEmail" value="Küldés">
							</p>
						</form>					
					</section>
			</main>	
		<footer>
			<p></p>
		</footer>
	</body>
</html>