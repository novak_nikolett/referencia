﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeWork
{
    class Control // 4. program működtetése, Model és View összekapcsolása
    {
        private View view;

        private int bornMonth, bornDay;
        private string astral;

        public Control(string name, int bornMonth, int bornDay, string food, string color, string planet1, string planet2, string randomText, int years, bool bD, string dateNow)
        {
            this.bornMonth = bornMonth;
            this.bornDay = bornDay;

            string getHoroscope(int bMonth, int bDay) 
            {
                string horoscope = "";

                if (bMonth == 3 && bDay >= 21 || bMonth == 4 && bDay <= 19) horoscope = "Kos";
                else if (bMonth == 4 && bDay >= 20 || bMonth == 5 && bDay <= 20) horoscope = "Bika";
                else if (bMonth == 5 && bDay >= 21 || bMonth == 6 && bDay <= 21) horoscope = "Ikrek";
                else if (bMonth == 6 && bDay >= 22 || bMonth == 7 && bDay <= 22) horoscope = "Rák";
                else if (bMonth == 7 && bDay >= 23 || bMonth == 8 && bDay <= 22) horoscope = "Oroszlán";
                else if (bMonth == 8 && bDay >= 23 || bMonth == 9 && bDay <= 22) horoscope = "Szűz";
                else if (bMonth == 9 && bDay >= 23 || bMonth == 10 && bDay <= 22) horoscope = "Mérleg";
                else if (bMonth == 10 && bDay >= 23 || bMonth == 11 && bDay <= 21) horoscope = "Skorpió";
                else if (bMonth == 11 && bDay >= 22 || bMonth == 12 && bDay <= 21) horoscope = "Nyilas";
                else if (bMonth == 12 && bDay >= 22 || bMonth == 1 && bDay <= 19) horoscope = "Bak";
                else if (bMonth == 1 && bDay >= 20 || bMonth == 2 && bDay <= 18) horoscope = "Vízöntő";
                else if (bMonth == 2 && bDay >= 19 || bMonth == 3 && bDay <= 20) horoscope = "Halak";

                return horoscope;
            }
            astral = getHoroscope(bornMonth, bornDay);

            view = new View(name, planet1, planet2, astral, randomText, years, bD, dateNow);
        }
    }
}
