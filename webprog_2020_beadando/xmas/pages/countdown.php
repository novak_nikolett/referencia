<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="UTF-8">
	<title>Visszaszámláló</title>

	<link rel="stylesheet" type="text/css" href="countdown_style.css">
	
</head>
	<body>


<?php
	function dateDiff($dformat, $endDate, $beginDate)
	{
		$date_parts2=explode($dformat, $endDate);
		$start_date=gregoriantojd($beginDate[0], $beginDate[1], $beginDate[2]);
		$end_date=gregoriantojd($date_parts2[0], $date_parts2[1], $date_parts2[2]);
	
		return $end_date - $start_date;
	}

	$date2="12/25/2020";

	$today=(int)date("d");
	$thismounth=(int)date("m");
	$thisyear=(int)date("Y");
	
	$realDate= array($thismounth, $today, $thisyear);

	print " Még " . dateDiff("/", $date2, $realDate) . " nap van karácsonyig!";
?>

	</body>
</html>