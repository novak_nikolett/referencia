﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeWork
{
    class Model // 3. adatok tárolása, kezelése
    {
        private Control control;

        private string food, color;
        private int bornYear, bornMonth, bornDay, thisYear, thisMonth, thisDay, years;
        private string planet1, planet2, randomText, dateNow, dayOW;
        private bool bD = false;

        DateTime todayDate = DateTime.Now;
        int day = (int)DateTime.Now.DayOfWeek;

        Random random = new Random();
        int randomPlanet1, randomPlanet2, randomRizsa;

        public Model(string name, int bornYear, int bornMonth, int bornDay, string food, string color)
        {
            this.food = food;
            this.color = color;
            this.bornYear = bornYear;
            this.bornMonth = bornMonth;
            this.bornDay = bornDay;

            // jelenlegi dátum kezelése

            thisYear = todayDate.Year;
            thisMonth = todayDate.Month;
            thisDay = todayDate.Day;

            years = thisYear - bornYear;

            switch (day)
            {
                case 1:
                    dayOW = "HÉTFŐ";
                    break;
                case 2:
                    dayOW = "KEDD";
                    break;
                case 3:
                    dayOW = "SZERDA";
                    break;
                case 4:
                    dayOW = "CSÜTÖRTÖK";
                    break;
                case 5:
                    dayOW = "PÉNTEK";
                    break;
                case 6:
                    dayOW = "SZOMBAT";
                    break;
                case 7:
                    dayOW = "VASÁRNAP";
                    break;
            }

            // randomgenerálás a listák elemeihez

            randomPlanet1 = random.Next(1, 14);
                randomPlanet2 = random.Next(1, 13);

            if (bornMonth == thisMonth && bornDay == thisDay)
            {
                randomRizsa = 9;
                bD = true;
            }
            else randomRizsa = random.Next(1, 9);

            planet1 = egitestek[randomPlanet1];
            planet2 = egitestek[randomPlanet2];
            randomText = rizsa1[randomRizsa] + food + rizsa2[randomRizsa] + color + rizsa3[randomRizsa];

            dateNow = thisMonth + "." + thisDay + "\n" + dayOW;

            control = new Control(name, bornMonth, bornDay, food, color, planet1, planet2, randomText, years, bD, dateNow);
        }

        // megjelenítendő szöveghez tartozó listák


        public List<string> rizsa1 = new List<string>() 
        {
            "Itt a lehetőség, hogy előrelépj a ranglétrán! Mosogatófiúból pakolósráccá, aktakukacból aktatologatóvá, mekis pénztárosból azzá a menő arccá, aki a kuponokat találja ki a következő hónapra! Ma a Hold szerencsével és elköteleződéssel áld meg, hogy bemehess a főnöködhöz, és bátran - de nem pofátlan módra - fizetésemelést kérhess! De ne üress hassal menj, ne légy morcos! Egyél egy tál ",
            "Megmászni a Himaláját? Megtudni a legfinomabb  ",
            "Barátaid száma, mint csillag az égen! Fantasztikus magja vagy a közösségednek, sugárzod boldogságod rájuk, mint a Napunk az ő fényét, melegét mindannyiunkra! Régi barátaidra is bármikor számíthatsz a mai nap, szervezz egy grillpartyt egy kis ",
            "A mai nappal hatalmas meteorit raj indult útjának, tedd hát te is ezt! Elérkezett a legmegfelelőbb pillanat, hogy meglátogasd a nagyit Alaszkában, hogy együtt fuss a kengurukkal Ausztráliában, megkeresd a maják elveszett kincsét, vagy leugorj cimborádhoz, Spongya Bobhoz a Balti tenger mélyére! Egyél egy finom ",
            "Ma nem lesz csillaghullás, nem fenyegetik a Földet Ausztrália-nagyságú meteoritok, tehát: otthon maradhatsz, és lazulhatsz. Rendelj egy tál ",
            "Unod már az egyhangúságot? Ma rádmosolyognak a csillagok, rendkívüli kreativitást bocsátanak rád, hogy igazi ikebana-mesterré válhass! Főzni sem késő megtanulni, ne csak ",
            "Jó hírem van: csillagaid megközelítettek egy fekete lyukat, ami az összes balszerencséd elnyelte! Irány a helyi lottózó, tipszmikszelde, és készülj fel, hogy pénz áll a házhoz! Ne csodálkozz hát, ha egész nap viszket a tenyered, nem bőrgomba okozza! De óvakodj, mert feltűnt az égen egy csoport kozmikus hulladék, ne árnyékolja be meggazdagodásod jövőjét: ne feledd megosztani javaidat, hívd meg családod egy fogás ",
            "Csillagaidat egy meteorraj vette körbe, hiába próbálnál ma bármi hasznosat csinálni, engedd el, add fel. Ez van. Jobb, ha megfogadod a tanácsom: inkább feküdj vissza aludni! A dolgok ma kiesnek a kezedből, mind képletesen, mind fizikailag: törsz-zúzol, mint egy dagadék elefánt a porcelánboltban, odaégeted a ",
            "Te aztán tudod, miből épül a kockás has és az izmos combok! Ha így folytatod a jövőben valamelyik olimpián találhatod magad, vagy legalább a megyeiben.. Gyorsabban futsz, mint egy hullócsillag, és kitartóbb vagy mint a Nap ragyogása! Ilyen kondíció mellett megengedhetsz magadnak minden nap egy tál ",
/*extra!*/  "Ne aggódj, hogy egy évvel bölcsebb lettél, öregebb mindenképp. Ha szerencséd van, és vannak barátaid, ma jó sok kedves szót bezsebelhetsz a köszöntőktől, ha sweet 16 lettél, talán még egy trabanttal is megajándékoznak az ősök, ha inkább közelítesz a 30-hoz, remélem nem unod még a Bocit. Reméljük a szülinapi tortád "
        };

        public List<string> rizsa2 = new List<string>()
        {
            "-t, de vigyázz, le ne edd kedvenc ",
            " titkos receptjét? Megtanulni hindiül? Tökéletesre vasalni a ",
            "-vel, egy hatalmas bulit, vagy egy strandolós napot, és dobd be a  ",
            "-t a Luvre előtt, mászd meg a Himaláját, kergess ",
            "-t, bújj a ",
            "-t tudj készíteni ennyi idősen.. Nem csíped a szépen rendezett",
            "-ra a vagyonodból. Aztán jöhet a ",
            "-dat, összemosod a ",
            "-t, és megveheted azt a kis ",
            " ízű lesz, a lufik pedig "
        };

        public List<string> rizsa3 = new List<string>()
        {
            " inged, pacásan nem tűnnél elég határozottnak!",
            " színű inged? Tökéletes gabonaköröket készíteni? Megszámolni az összes rizsszemet otthon? Ma bármit véghez vihetsz! Csak indítsd útjának határtalan képzelőerőd, és máris az öledbe hullik a sült galamb, hála a csillagok együttállásának! Ma nincs számodra lehetetlen, fejezd hát be azt a nyavajás beadandót!!!",
            " színű úszógumit, mindenki imádni fogja! Esti zárásnak elengedhetetlen egy tábortűz, egy gitár, és persze a dalok királya, Te! Jó szórakozást!",
            " színű egyszarvúkat Csodaországban! Közben ne felejts el utazási pontokat gyűjteni, holnap is lesz egy nap..",
            " színű pizsidbe, és kapcsold be a Netflixet (nem fizetett hirdetés). Eljött a lehetőség, hogy ledarálj egy 12 évados sorozatot, elolvasd a Harry Potter regényeket, és otthon maradj. Pont, mint a 2020-as tavaszi kijárási korlátozás alatt, emlékszel, milyen jó is volt?! Mottó a mai napra: 'bármit, csak ne hasznosat!'",
            " virágokat? Sebaj, tudok még pár fantasztikus lehetőséget ezen hirtelen alkotóképesség kamatoztatására: ott az origami, vagy kezdj bélyegek gyűjtésébe (egyszer még sokat érnek majd), telepíts akváriumot, indíts blogot, vagy próbáld ki a tandemugrást!",
            " színű Porsche!",
            " színű kedvenc pulcsid a jujubapiros alsóneműddel, munkába menet egy fekete macska-falka halad át előtted az úton, folytassam?",
            " színű ruhát, amit kinéztél a múlt héten! Aztán csak csínján a szteroidokkal;)",
            " színűek, és nem mandarin tangó, mert lássuk be, az elég cikis lenne. HBD!"
        };

        public List<string> egitestek = new List<string>()
        { "Merkúr", "Vénusz", "Föld", "Mars", "Jupiter", "Szaturnusz",
          "Uránusz", "Neptunusz", "Pluto", "Ceres", "Phobosz", "Europé", "Prométheusz" };

        public List<string> horoszkopok = new List<string>()
        { "Kos", "Bika", "Ikrek", "Rák", "Oroszlán", "Szűz", "Mérleg", "Skorpió", "Nyilas", "Bak", "Vízöntő", "Halak" };

    }
}
