<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="UTF-8">
	<title>Tökéletes tojáslikőr recept</title>

	<link rel="stylesheet" type="text/css" href="tojaslikor_style.css">
	
</head>
	<body>
	<section>
		<header>
			<h1>Tojáslikőr</h1>
		</header>
			<main>		
					<section>
						<h2>Hozzávalók</h2>
						<p>
						<nav>
							<ul>
								<li>500 ml tej</li>
								<li>250 ml habtejszín</li>
								<li>vanília</li>
								<li>100 g porcukor</li>
								<li>5 db tojássárgája</li>
								<li>250 ml rum</li>
								<li>150 g porcukor</li>
							</ul>
						</nav>
						</p>
					</section>
					<section>
						<h2>Elkészítés</h2>						
							<p>A tejet és a tejszínt összeöntjük a vaníliával és a 100 g porcukorral.</p>
							<p>Felforraljuk, majd félretesszük.</p>
							<p>A tojássárgájákat kikeverjük a másik adag porcukorral.</p>
							<p>A forró tejet vékony sugárban folyamatos kevergetés mellett a tojássárgájához öntjük, majd hűlni hagyjuk.</p>
							<p>Hozzáöntjük a szeszt, majd ha kihűlt, üvegben, hűtőben tároljuk.</p>
					</section>
			</main>	
		<footer>
			<p></p>
		</footer>
	</body>
</html>