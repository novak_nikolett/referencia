﻿namespace HomeWork
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.theRizsa = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // theRizsa
            // 
            this.theRizsa.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.theRizsa.BackColor = System.Drawing.Color.Transparent;
            this.theRizsa.Font = new System.Drawing.Font("Engravers MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.theRizsa.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.theRizsa.Location = new System.Drawing.Point(12, 53);
            this.theRizsa.MaximumSize = new System.Drawing.Size(600, 350);
            this.theRizsa.Name = "theRizsa";
            this.theRizsa.Size = new System.Drawing.Size(600, 350);
            this.theRizsa.TabIndex = 4;
            this.theRizsa.Text = "the rizsa";
            this.theRizsa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.theRizsa.Click += new System.EventHandler(this.label3_Click);
            // 
            // date
            // 
            this.date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.date.BackColor = System.Drawing.Color.Transparent;
            this.date.Font = new System.Drawing.Font("Freehand521 BT", 30F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.date.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.date.Location = new System.Drawing.Point(679, 394);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(283, 148);
            this.date.TabIndex = 14;
            this.date.Text = "mm . dd nap";
            this.date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form2
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::HomeWork.Properties.Resources.pic5;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.date);
            this.Controls.Add(this.theRizsa);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Napi jóslatod";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label theRizsa;
        private System.Windows.Forms.Label date;
    }
}