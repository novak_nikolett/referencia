﻿namespace HomeWork
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.fName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.bornDate = new System.Windows.Forms.TextBox();
            this.favFood = new System.Windows.Forms.TextBox();
            this.favCol = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.goBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fName
            // 
            this.fName.Font = new System.Drawing.Font("Engravers MT", 7F, System.Drawing.FontStyle.Bold);
            this.fName.Location = new System.Drawing.Point(294, 300);
            this.fName.Name = "fName";
            this.fName.Size = new System.Drawing.Size(172, 18);
            this.fName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Engravers MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(30, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(297, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ahhoz, hogy megtudd,";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Engravers MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(30, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(364, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "mit tartogatnak számodra";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Engravers MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(30, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(436, 19);
            this.label3.TabIndex = 3;
            this.label3.Text = "néhány adatra lesz szükségünk:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Engravers MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Location = new System.Drawing.Point(30, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(328, 19);
            this.label4.TabIndex = 4;
            this.label4.Text = " a csillagok a mai napra,";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Engravers MT", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.Location = new System.Drawing.Point(40, 300);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 14);
            this.label5.TabIndex = 5;
            this.label5.Text = "keresztneved:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Engravers MT", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(40, 349);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(203, 14);
            this.label6.TabIndex = 6;
            this.label6.Text = "születési dátumod:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Engravers MT", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label7.Location = new System.Drawing.Point(82, 363);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 14);
            this.label7.TabIndex = 7;
            this.label7.Text = "(0000.00.00)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Engravers MT", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label8.Location = new System.Drawing.Point(40, 413);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(171, 14);
            this.label8.TabIndex = 8;
            this.label8.Text = "kedvenc ételed:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Engravers MT", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label9.Location = new System.Drawing.Point(40, 469);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(167, 14);
            this.label9.TabIndex = 9;
            this.label9.Text = "kedvenc színed:";
            // 
            // bornDate
            // 
            this.bornDate.Font = new System.Drawing.Font("Engravers MT", 7F, System.Drawing.FontStyle.Bold);
            this.bornDate.Location = new System.Drawing.Point(294, 349);
            this.bornDate.Name = "bornDate";
            this.bornDate.Size = new System.Drawing.Size(172, 18);
            this.bornDate.TabIndex = 10;
            // 
            // favFood
            // 
            this.favFood.Font = new System.Drawing.Font("Engravers MT", 7F, System.Drawing.FontStyle.Bold);
            this.favFood.Location = new System.Drawing.Point(294, 407);
            this.favFood.Name = "favFood";
            this.favFood.Size = new System.Drawing.Size(172, 18);
            this.favFood.TabIndex = 11;
            // 
            // favCol
            // 
            this.favCol.Font = new System.Drawing.Font("Engravers MT", 7F, System.Drawing.FontStyle.Bold);
            this.favCol.Location = new System.Drawing.Point(294, 463);
            this.favCol.Name = "favCol";
            this.favCol.Size = new System.Drawing.Size(172, 18);
            this.favCol.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Vivaldi", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label10.Location = new System.Drawing.Point(544, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(428, 76);
            this.label10.TabIndex = 13;
            this.label10.Text = "Napi horoszkópod";
            // 
            // goBtn
            // 
            this.goBtn.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.goBtn.Font = new System.Drawing.Font("Engravers MT", 9F, System.Drawing.FontStyle.Bold);
            this.goBtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.goBtn.Location = new System.Drawing.Point(206, 511);
            this.goBtn.Name = "goBtn";
            this.goBtn.Size = new System.Drawing.Size(94, 25);
            this.goBtn.TabIndex = 14;
            this.goBtn.Text = "Mehet!";
            this.goBtn.UseVisualStyleBackColor = false;
            this.goBtn.Click += new System.EventHandler(this.goBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.goBtn);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.favCol);
            this.Controls.Add(this.favFood);
            this.Controls.Add(this.bornDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fName);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tudd meg, mi vár rád ma!";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox bornDate;
        private System.Windows.Forms.TextBox favFood;
        private System.Windows.Forms.TextBox favCol;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button goBtn;
    }
}

